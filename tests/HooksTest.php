<?php
Namespace dgifford\Tag;




/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class HooksTest extends \PHPUnit\Framework\TestCase
{
	public function testOutputStringHook()
	{
		$tag = new Tag( 'div', 'My content' );

		$this->assertSame( '<div>My content</div>', $tag->render(false) );

		$tag->addHook( 'output_string', function( $value ) { return strtoupper($value); } );

		$this->assertSame( '<DIV>MY CONTENT</DIV>', $tag->render(false) );
	}



	public function testOutputStringHookPriorities()
	{
		$tag = new Tag( 'div', 'My content' );

		$this->assertSame( '<div>My content</div>', $tag->render(false) );

		$tag->addHook( 'output_string', function( $value ) { return $value . ' foo'; }, 10 );

		$tag->addHook( 'output_string', function( $value ) { return strtoupper($value); }, 1 );

		$this->assertSame( '<DIV>MY CONTENT</DIV> foo', $tag->render(false));
	}



	public function testBeforeRenderHook()
	{
		$tag = new Tag( 'div', 'My content' );

		$this->assertSame( '<div>My content</div>', $tag->render(false) );

		$tag->addHook( 'before_render', function( $obj ) { return $obj->setAttribute('class', 'my_class'); }, 10 );

		$tag->addHook( 'before_render', function( $obj ) { return $obj->setAttribute('id', 'my_id'); }, 1 );

		$this->assertSame( '<div class="my_class" id="my_id">My content</div>', $tag->render(false) );
	}



	public function testInitialOutputStringHook()
	{
		$tag = new Tag( 'div', 'My content' );

		$this->assertSame( '<div>My content</div>', $tag->render(false) );

		$tag->addHook( 'initial_output_string', function( $str, $obj ) { return $obj->getName(); } );

		$this->assertSame( 'div<div>My content</div>', $tag->render(false) );
	}
}