<?php
Namespace dgifford\Tag;




/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class RepeatTest extends \PHPUnit\Framework\TestCase
{
	public function testCreateUnorderedListWithStrings()
	{
		$tag = new Tag( 'ul' );

		$tag->repeat( 'li', [ 
			'string 1', 
			'string 2', 
			'string 3',
		]);

		$this->assertSame( '<ul><li>string 1</li><li>string 2</li><li>string 3</li></ul>', $tag->render(false) );
	}


	public function testRepeatTagObjectWithAttributes()
	{
		$html = new Tag( 'ul' );

		$html->repeat( new Tag( 'li', ['class' => 'common_class']), [ 
			'string 1', 
			'string 2', 
			[['id' => 'my_id'], 'string 3'],
		]);

		$this->assertSame( '<ul><li class="common_class">string 1</li><li class="common_class">string 2</li><li class="common_class" id="my_id">string 3</li></ul>', $html->render(false) );
	}



	public function testRepeatTagObjectWithAttributesAndMergeAttributes()
	{
		$html = new Tag( 'ul' );

		$html->repeat( new Tag( 'li', ['class' => 'common_class']), [ 
			'string 1', 
			'string 2', 
			[['class' => 'my_class'], 'string 3'],
		]);

		$this->assertSame( '<ul><li class="common_class">string 1</li><li class="common_class">string 2</li><li class="common_class my_class">string 3</li></ul>', $html->render(false) );
	}



	public function testRepeatTagObjectWithAttributesAndReplaceAttributes()
	{
		$html = new Tag( 'ul' );

		$html->repeat( new Tag( 'li', ['class' => 'common_class']), [ 
			'string 1', 
			'string 2', 
			[['class' => 'my_class'], 'string 3'],
		], false);

		$this->assertSame( '<ul><li class="common_class">string 1</li><li class="common_class">string 2</li><li class="my_class">string 3</li></ul>', $html->render(false) );
	}



	public function testStartUnorderedListThenAddStrings()
	{
		$tag = new Tag( 'ul' );

		$tag->add( 'li', 'First Item' );

		$tag->repeat( 'li', [ 
			'string 1', 
			'string 2', 
			'string 3',
		]);

		$this->assertSame( '<ul><li>First Item</li><li>string 1</li><li>string 2</li><li>string 3</li></ul>', $tag->render(false) );
	}



	public function testRepeatWithMixedData()
	{
		$tag = new Tag( 'ul' );

		$tag->repeat( 'li', [ 
			'string 1', 
			new Tag('a', ['href' => 'http://google.com'], 'Google' ), 
			[['class' => 'my_class'], 'string 3'],
		]);

		$this->assertSame( '<ul><li>string 1</li><li><a href="http://google.com">Google</a></li><li class="my_class">string 3</li></ul>', $tag->render(false) );
	}



	public function testRepeatUsingFilter()
	{
		$html = new Tag( 'div', 'div content' );

		$html->add( new Tag( 'ul' ) );

		$html->add( new Tag( 'ul', ['id' => 'target'] ) );

		$html->filter(['id' => 'target'])->repeat( 'li', [ 
			'string 1', 
			new Tag('a', ['href' => 'http://google.com'], 'Google' ), 
			[['class' => 'my_class'], 'string 3'],
		]);

		$this->assertSame( '<div>div content<ul></ul><ul id="target"><li>string 1</li><li><a href="http://google.com">Google</a></li><li class="my_class">string 3</li><li>string 1</li><li><a href="http://google.com">Google</a></li><li class="my_class">string 3</li></ul></div>', $html->render(false) );
	}



	public function testRepeatWithoutEscapingContent( $value = '' )
	{
		$html = new Tag( 'div' );

		$html->repeat( (new Tag( 'p' ))->setEscContent( false ), ['line 1', 'line 2', '<strong>line 3</strong>'] );

		$this->assertSame( '<div><p>line 1</p><p>line 2</p><p><strong>line 3</strong></p></div>', $html->render(false) );
	}

/*

*/
}