<?php
Namespace dgifford\Tag;




/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class TagTest extends \PHPUnit\Framework\TestCase
{
	public function setUp()
	{
		$inner = new Tag( 'div', ['class' => 'inner'] );

		$inner->setContent([
			['div', 'some content'],
			['div', ['class' => 'inner', 'id' => 'unique_div_id'], 'more content'],
			['div', 'more content'],
			['span', 'spanned content'],
			['div', 'more content'],
			['span', 'spanned content'],
			['div', 'more content'],
			['span', ['class' => 'inner', 'id' => 'unique_span_id'], 'spanned content'],
		]);

		$this->tag = new Tag( 'div' );

		$this->tag->setContent([
			['div', 'some content'],
			['div', ['class' => 'inner'], 'more content'],
			$inner, 
			['div', 'more content'],
			['div', 'more content'],
			['div', 'more content'],
		]);
	}



	public function testEmptyConstructor()
	{
		$tag = new Tag;

		$this->assertSame( '', $tag->render( false ) );
	}



	public function testMultipleObjectsInConstructor()
	{
		$tag = new Tag('div', [], [
			['Level 1'],
			new Tag('div', [], [
				['Level 2'],
				new Tag('div', 'Level 3'),
				['Level 2'],
			]),
			['Level 1'],
		]);

		$this->assertSame( '<div>Level 1<div>Level 2<div>Level 3</div>Level 2</div>Level 1</div>', $tag->render( false ) );
	}



	public function testContentWithNoWrappingTag()
	{
		$tag = new Tag;

		$tag->addContent('<p>my content</p>', false );
		$tag->addContent('<p>my content</p>', false );

		$this->assertSame( '<p>my content</p><p>my content</p>', $tag->render( false ) );
	}



	public function testArgumentsInConstructor()
	{
		$tag = new Tag( 'p', ['class' => 'my_class'], 'my content' );

		$this->assertSame( '<p class="my_class">my content</p>', $tag->render( false ) );
	}



	public function testSetTwoArgumentsSecondAsContent()
	{
		$tag = new Tag( 'p', 'my content' );

		$this->assertSame( '<p>my content</p>', $tag->render( false ) );
	}



	public function testSetTwoArgumentsSecondAsArray()
	{
		$tag = new Tag( 'p', ['class' => 'my_class'] );

		$this->assertSame( '<p class="my_class"></p>', $tag->render( false ) );
	}



	public function testCreateVoidElement()
	{
		$tag = new Tag( 'input', ['value' => 'my_value'] );

		$this->assertSame( '<input value="my_value"/>', $tag->render( false ) );
	}



	public function testChangeProperties()
	{
		$tag = new Tag( 'input', ['value' => 'my_value'] );

		$tag->set( 'p', ['class' => 'my_class'] );

		$this->assertSame( '<p class="my_class" value="my_value"></p>', $tag->render( false ) );
	}



	public function testChangeAttributes()
	{
		$tag = new Tag;

		$tag->set( 'p', ['class' => 'my_class'] );

		$this->assertSame( '<p class="my_class"></p>', $tag->render( false ) );

		$tag->setAttribute( 'id', 'my_id' );

		$this->assertSame( '<p class="my_class" id="my_id"></p>', $tag->render( false ) );

		$this->assertTrue( $tag->hasAttributes() );

		$this->assertFalse( $tag->isVoid() );
	}



	public function testBooleanAttributes()
	{
		$tag = new Tag( 'input', ['checked' => true ] );

		$this->assertSame( '<input checked="checked"/>', $tag->render( false ) );

		$tag->setAttribute( 'checked', false );

		$this->assertSame( '<input/>', $tag->render( false ) );
	}



	public function testClassAttributes()
	{
		$tag = new Tag( 'input', ['class' => 'foo bar' ] );

		$this->assertSame( '<input class="foo bar"/>', $tag->render( false ) );

		$this->assertTrue( $tag->matches( ['class' => 'foo bar' ] ) );

		$this->assertTrue( $tag->matches( ['class' => 'bar foo' ] ) );

		$this->assertTrue( $tag->matches( ['class' => ['foo', 'bar'] ] ) );

	}



	public function testChangeXhtmlCompatibility()
	{
		$tag = new Tag;

		$tag->set( 'input', false );

		$this->assertSame( '<input>', $tag->render( false ) );

		$this->assertTrue( $tag->isEmpty() );

		$this->assertTrue( $tag->isVoid() );

		$this->assertFalse( $tag->hasAttributes() );
	}



	public function testUseInvalidName()
	{
		$tag = new Tag( 'inp ut[=' );

		$this->assertSame( '<input/>', $tag->render( false ) );

		$this->assertTrue( $tag->isXHTMLCompatible() );
	}



	public function testUsingInvalidAttributes()
	{
		$tag = new Tag( 'div', [
			'class' => '" onmouseover=alert(/Hello/)',
			'id' => '\'\';!--"<XSS>=&{()}',
		]);

		$this->assertSame( '<div class="&quot; onmouseover=alert(/Hello/)" id="&#039;&#039;;!--&quot;&lt;XSS&gt;=&amp;{()}"></div>', $tag->render( false ) );
	}



	public function testSetStringContentEscaped()
	{
		$tag = new Tag( 'div' );

		$tag->setContent( 'some <strong>content</strong>' );

		$this->assertSame( '<div>some &lt;strong&gt;content&lt;/strong&gt;</div>', $tag->render( false ) );
	}



	public function testSetStringContentNotEscaped()
	{
		$tag = new Tag( 'div' );

		$tag->setContent( 'some <strong>content</strong>', false );

		$this->assertSame( '<div>some <strong>content</strong></div>', $tag->render( false ) );

		$tag->setContent( 'some <strong>content</strong>' );


		$this->assertSame( '<div>some &lt;strong&gt;content&lt;/strong&gt;</div>', $tag->render( false ) );

		// Set default escaping
		$tag->setEscContent( false );

		$tag->setContent( 'some <strong>content</strong>' );

		$this->assertSame( '<div>some <strong>content</strong></div>', $tag->render( false ) );
	}



	public function testSetContentWithThreeTagArguments()
	{
		$tag = new Tag( 'div' );

		$tag->setContent( 'div', ['class' => 'inner'], 'some content' );

		$this->assertSame( '<div><div class="inner">some content</div></div>', $tag->render( false ) );
	}



	public function testSetContentWithTwoTagArguments()
	{
		$tag = new Tag( 'div' );

		$tag->setContent( 'div', ['class' => 'inner'] );

		$this->assertSame( '<div><div class="inner"></div></div>', $tag->render( false ) );
	}



	public function testSetContentWithTwoTagArgumentsAgain()
	{
		$tag = new Tag( 'div' );

		$tag->setContent( 'div', 'some content' );

		$this->assertSame( '<div><div>some content</div></div>', $tag->render( false ) );
	}



	public function testSetContentWithTagObjectArgument()
	{
		$tag = new Tag( 'div' );

		$inner = new Tag( 'div', ['class' => 'inner'], 'some content' );

		$tag->setContent( $inner );

		$this->assertSame( '<div><div class="inner">some content</div></div>', $tag->render( false ) );
	}



	public function testSetContentWithArrayArgument()
	{
		$tag = new Tag( 'div' );

		$inner = new Tag( 'div', ['class' => 'inner'], 'some content' );

		$tag->setContent([
			[$inner],
			['<div>More content</div>', false],
			[ 'div', 'even more content' ],
		]);

		$this->assertSame( '<div><div class="inner">some content</div><div>More content</div><div>even more content</div></div>', $tag->render( false ) );
	}



	public function testSetContentWithArrayOfStrings()
	{
		$tag = new Tag( 'div' );

		$tag->setContent([
			'Some content ',
			'More content ',
			'Even more content ',
		]);

		$this->assertSame( '<div>Some content More content Even more content </div>', $tag->render( false ) );
	}



	public function testSetContentWithMixedArrayOfRawHtmlandStrings()
	{
		$tag = new Tag( 'div' );

		$tag->setContent([
			'Some content ',
			['<span>More content</span> ', false],
			'Even more content ',
		]);

		$this->assertSame( '<div>Some content <span>More content</span> Even more content </div>', $tag->render( false ) );
	}



	public function testContentFormatPreserved()
	{
		$tag = new Tag( 'div' );

		$inner = new Tag( 'div', ['class' => 'inner'], 'some content' );

		$tag->setContent([
			[$inner],
			['<div>More content</div>', false],
			[ 'div', 'even more content' ],
		]);

		$this->assertTrue( $tag->content[0] instanceof Tag );

		$this->assertSame( ['<div>More content</div>', false], $tag->content[1] );

		$this->assertTrue( $tag->content[2] instanceof Tag );
	}



	public function testAddContentWithStringArgument()
	{
		$tag = new Tag( 'div', 'Some content.' );

		$tag->addContent( 'More content.' );

		$this->assertSame( '<div>Some content.More content.</div>', $tag->render( false ) );
	}



	public function testAddContentWithTagObjectArgument()
	{
		$tag = new Tag( 'div', 'Some content.' );

		$inner = new Tag( 'div', ['class' => 'inner'], 'some content' );

		$tag->addContent( $inner );

		$this->assertSame( '<div>Some content.<div class="inner">some content</div></div>', $tag->render( false ) );
	}



	public function testCreateTagWithIntegerArgument()
	{
		$tag = new Tag( 'div', 0.1234 );

		$this->assertSame( '<div>0.1234</div>', $tag->render( false ) );
	}



	public function testAppendAfterExistingContent()
	{
		$tag = new Tag( 'div', 'my content' );

		$tag->append( 'div', 'my content' );

		$this->assertSame( '<div>my content<div>my content</div></div>', $tag->render( false ) );
	}



	public function testInsertBeforeExistingContent()
	{
		$tag = new Tag( 'div', 'my content' );

		$tag->insert( 0, '(more content) ' );

		$tag->insert( 0, '(even more content) ' );

		$tag->insert( 1, '(yet more content) ' );

		$tag->insert( 3, ['<span>Yo! More content</span> ', false] );

		$this->assertSame( '<div>(even more content) (yet more content) (more content) <span>Yo! More content</span> my content</div>', $tag->render( false ) );
	}



	public function testPrependToStartOfContent()
	{
		$tag = new Tag( 'div', 'my content' );

		$tag->append( '(more content)' );

		$tag->prepend( '(prepended content) ' );

		$this->assertSame( '<div>(prepended content) my content(more content)</div>', $tag->render( false ) );
	}



	public function testMatches()
	{
		$tag = new Tag( 'div', ['class' => 'inner', 'id' => 'my_id' ] );

		$this->assertTrue( $tag->matches( 'div' ) );

		$this->assertFalse( $tag->matches( 'span' ) );

		$this->assertTrue( $tag->matches(['class' => 'inner', 'id' => 'my_id' ]) );

		$this->assertTrue( $tag->matches(['id' => 'my_id', 'class' => 'inner', ]) );

		$this->assertTrue( $tag->matches( 'div', ['id' => 'my_id', 'class' => 'inner', ]) );

		$this->assertFalse( $tag->matches( 'span', ['id' => 'my_id', 'class' => 'inner', ]) );

		$tag = new Tag( 'div', ['class' => ['foo', 'bar'], 'id' => 'my_id' ] );

		$this->assertTrue( $tag->matches( 'div', ['class' => ['bar', 'foo' ]] ) );

		$this->assertTrue( $tag->matches( 'div', ['class' => ['bar' ]] ) );

		$this->assertTrue( $tag->matches( 'div', ['class' => 'bar'] ) );

		$this->assertTrue( $tag->matches( 'div', ['class' => 'foo'] ) );

		$this->assertFalse( $tag->matches( 'span', ['class' => 'foo'] ) );
	
		$tag = new Tag( 'div', ['class' => 'foo bar', 'id' => 'my_id' ] );

		$this->assertTrue( $tag->matches( 'div', ['class' => ['bar', 'foo' ]] ) );
	}



	public function testAttributeEquals()
	{
		$tag = new Tag( 'div', ['class' => ['foo', 'bar'], 'id' => 'my_id', 'required' => true ] );

		$this->assertFalse( $tag->attributeEquals( 'class', 'inner' ) );

		$this->assertTrue( $tag->attributeEquals( 'class', ['foo', 'bar'] ) );

		$this->assertTrue( $tag->attributeEquals( 'class', ['bar','foo', ] ) );

		$this->assertFalse( $tag->attributeEquals( 'class', ['bar','foo', ], true ) );

		$this->assertTrue( $tag->attributeEquals( 'class', 'foo bar' ) );

		$this->assertFalse( $tag->attributeEquals( 'class', 'foo' ) );

		$this->assertTrue( $tag->attributeEquals( 'required', true ) );
	}



	public function testFind()
	{
		$this->assertSame( '<div><div>some content</div><div class="inner">more content</div><div class="inner"><div>some content</div><div class="inner" id="unique_div_id">more content</div><div>more content</div><span>spanned content</span><div>more content</div><span>spanned content</span><div>more content</div><span class="inner" id="unique_span_id">spanned content</span></div><div>more content</div><div>more content</div><div>more content</div></div>', $this->tag->render(false) );

		// Find and change all 3 occurances of span
		foreach( $this->tag->find( 'span' ) as $obj )
		{
			$obj->setContent('CHANGED SPANNED CONTENT');
		}

		$this->assertSame( '<div><div>some content</div><div class="inner">more content</div><div class="inner"><div>some content</div><div class="inner" id="unique_div_id">more content</div><div>more content</div><span>CHANGED SPANNED CONTENT</span><div>more content</div><span>CHANGED SPANNED CONTENT</span><div>more content</div><span class="inner" id="unique_span_id">CHANGED SPANNED CONTENT</span></div><div>more content</div><div>more content</div><div>more content</div></div>', $this->tag->render(false) );
	

		// Find and change 1 span
		foreach( $this->tag->find( '', ['id' => 'unique_span_id'] ) as $obj )
		{
			$obj->setContent('CHANGED SPANNED CONTENT AGAIN');
		}

		$this->assertSame( '<div><div>some content</div><div class="inner">more content</div><div class="inner"><div>some content</div><div class="inner" id="unique_div_id">more content</div><div>more content</div><span>CHANGED SPANNED CONTENT</span><div>more content</div><span>CHANGED SPANNED CONTENT</span><div>more content</div><span class="inner" id="unique_span_id">CHANGED SPANNED CONTENT AGAIN</span></div><div>more content</div><div>more content</div><div>more content</div></div>', $this->tag->render(false) );


		// Find 0 elements
		foreach( $this->tag->find( 'div', ['class' => 'foo'] ) as $obj )
		{
			$obj->setContent('CHANGED SPANNED CONTENT AGAIN');
		}

		$this->assertSame( '<div><div>some content</div><div class="inner">more content</div><div class="inner"><div>some content</div><div class="inner" id="unique_div_id">more content</div><div>more content</div><span>CHANGED SPANNED CONTENT</span><div>more content</div><span>CHANGED SPANNED CONTENT</span><div>more content</div><span class="inner" id="unique_span_id">CHANGED SPANNED CONTENT AGAIN</span></div><div>more content</div><div>more content</div><div>more content</div></div>', $this->tag->render(false) );
	}



	public function testFilter()
	{
		$this->assertSame( '<div><div>some content</div><div class="inner">more content</div><div class="inner"><div>some content</div><div class="inner" id="unique_div_id">more content</div><div>more content</div><span>spanned content</span><div>more content</div><span>spanned content</span><div>more content</div><span class="inner" id="unique_span_id">spanned content</span></div><div>more content</div><div>more content</div><div>more content</div></div>', $this->tag->render(false) );

		$this->tag->filter(['id' => 'unique_div_id'])->set('div', 'CHANGED CONTENT');
		
		$this->assertSame( '<div><div>some content</div><div class="inner">more content</div><div class="inner"><div>some content</div><div class="inner" id="unique_div_id">CHANGED CONTENT</div><div>more content</div><span>spanned content</span><div>more content</div><span>spanned content</span><div>more content</div><span class="inner" id="unique_span_id">spanned content</span></div><div>more content</div><div>more content</div><div>more content</div></div>', $this->tag->render(false) );

		// Filter still applied
		$this->tag->setContent('foo bar');

		$this->assertSame( '<div><div>some content</div><div class="inner">more content</div><div class="inner"><div>some content</div><div class="inner" id="unique_div_id">foo bar</div><div>more content</div><span>spanned content</span><div>more content</div><span>spanned content</span><div>more content</div><span class="inner" id="unique_span_id">spanned content</span></div><div>more content</div><div>more content</div><div>more content</div></div>', $this->tag->render(false) );

		// Filter still applied
		$this->tag->clear();

		$this->assertSame( '<div><div>some content</div><div class="inner">more content</div><div class="inner"><div>some content</div><div class="inner" id="unique_div_id"></div><div>more content</div><span>spanned content</span><div>more content</div><span>spanned content</span><div>more content</div><span class="inner" id="unique_span_id">spanned content</span></div><div>more content</div><div>more content</div><div>more content</div></div>', $this->tag->render(false) );

		// Clear the filter
		$this->tag->clearFilters();
		$this->tag->setContent('foo bar');

		$this->assertSame( '<div>foo bar</div>', $this->tag->render(false) );
	}



	public function testMethodsChainable()
	{
		$tag = new Tag( 'div', 'div content' );

		$this->assertSame( '<div>div content</div>', $tag->render(false) );

		$tag->set( 'span' )->setContent( 'span content')->setAttribute( 'class', 'my_class' );

		$this->assertSame( '<span class="my_class">span content</span>', $tag->render(false) );

		$tag->set( 'div', 'div content' )->append( 'span', 'span content' )->filter( 'span' )->clear()->clearFilters();

		$this->assertSame( '<div class="my_class">div content<span></span></div>', $tag->render(false) );

		$tag->setContent( new Tag( 'span', 'span content' ) )->append( 'span', 'another span' );

		$this->assertSame( '<div class="my_class"><span>span content</span><span>another span</span></div>', $tag->render(false) );
	}



	public function testMergeAttributes()
	{
		$tag = new Tag( 'div', ['id' => 'my_id', 'class' => 'my_class'] );

		$this->assertSame( '<div class="new_class" id="my_id"></div>', $tag->mergeAttributes(['class' => 'new_class'])->render(false) );
	}



	public function testAddNewClass()
	{
		$tag = new Tag( 'div', ['id' => 'my_id', 'class' => 'my_class'] );

		$this->assertSame( '<div class="my_class new_class" id="my_id"></div>', $tag->addClass( 'new_class' )->render(false) );
	}



	public function testAddExistingClassString()
	{
		$tag = new Tag( 'div', ['id' => 'my_id', 'class' => 'my_class'] );

		$this->assertSame( '<div class="my_class" id="my_id"></div>', $tag->addClass( 'my_class' )->render(false) );
	}



	public function testAddExistingClassArray()
	{
		$tag = new Tag( 'div', ['id' => 'my_id', 'class' => ['my_class']] );

		$this->assertSame( '<div class="my_class" id="my_id"></div>', $tag->addClass( ['my_class'] )->render(false) );
	}



	public function testAddExistingAndNewClassArray()
	{
		$tag = new Tag( 'div', ['id' => 'my_id', 'class' => 'my_class'] );

		$this->assertSame( '<div class="my_class new_class" id="my_id"></div>', $tag->addClass( ['my_class', 'new_class' ] )->render(false) );
	}



	/**
     * @expectedException \BadMethodCallException
     */
    public function testCallingNonExistentMethod()
	{
		$tag = new Tag;

		$tag->FooBar( ['my_class', 'new_class' ] );
	}



	public function testAttributesAlphabetical()
	{
		$tag = new Tag( 'div', ['style' => 'border: 1px solid #f00;', 'id' => 'my_id', 'class' => 'my_class', ] );

		$this->assertSame( '<div class="my_class" id="my_id" style="border: 1px solid #f00;"></div>', $tag->render(false) );
	}
}