<?php
Namespace dgifford\Tag;

/*
	Self-contained utility class for creating HTML tags.


	Copyright (C) 2016  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */





class Tag
{
	Use \dgifford\Traits\HooksTrait;



	// The name of the HTML tag
	public $name = '';

	// The attributes for the HTML tag
	public $attributes = [];

	// An array to hold the content for the tag
	// Content can be:
	// 	1. An array containing a string and boolean (whether to escape the string)
	// 	2. A Tag object
	public $content = [];

	// Whether to include a slash before closing > in void elements for compatibility with XHTML
	// http://stackoverflow.com/questions/1946426/html-5-is-it-br-br-or-br
	public $xhtml_compatible = true;

	// Void elements
	protected $void_elements = [ 'area', 'base', 'br', 'col', 'embed', 'hr', 'img', 'input', 'link', 'menuitem', 'meta', 'param', 'source', 'track', 'wbr' ];

	// Encoding used
	public $encoding = 'UTF-8';

	// Array to hold name and attribute filters used by filter()
	protected $filters = [];

	// Default content escaping
	protected $esc_content = true;







	/////////////////////////////////////////////////////
	// Setting/ editing content
	/////////////////////////////////////////////////////



	/**
	 * Arguments in constructor can be used to set properties of the tag.
	 * See parseArguements.
	 * 
	 * @return object            Allows methods to be chained
	 */
	public function __construct()
	{
		$this->parseArguements( func_get_args() );

		return $this;
	}



	/**
	 * Magic method allows methods to be applied to child tags
	 * if a filter is set, or just to parent tag.
	 * 
	 * @param  string $name      Function name
	 * @param  array  $arguments Arguments passed to method
	 * @return object            Allows methods to be chained
	 */
	public function __call( $name, $args )
	{
		if( $this->hasFilters() )
		{
			$this->applyToChildren( $name, $args );
		}
		else
		{
			switch( $name )
			{
				// Set the tag name, attributes and content.
				case 'set':

					$this->parseArguements( $args );

				break;



				// Set the content.
				case 'setContent':

					$this->clear();

					call_user_func_array( [ $this, 'addContent' ], $args );

				break;



				// Clear all content within the tag.
				case 'clear':
				case 'clearContent':

					$this->content = [];

				break;



				// Add content to the tag.
				case 'addContent':
				case 'add':
				case 'append':

					$this->content[] = $this->convertContent( $args );

				break;



				// Add an array of content items.
				case 'addMultiple':

					if( count($args) == 1 and is_array($args[0]) )
					{
						foreach( $args[0] as $key => $content )
						{
							if( !is_array($content) )
							{
								$content = [$content];
							}

							call_user_func_array([ $this, 'addContent'], $content );
						}
					}

				break;



				// Insert the given tag/content properties before the index given.
				case 'insert':

					if( is_int( $args[0] ) and array_key_exists( $args[0], $this->content ) and !empty($args[1]) )
					{
						if( !is_array($args[1]) )
						{
							$args[1] = [$args[1]];
						}
						
						$args[1] = $this->convertContent( $args[1] );

						array_splice( $this->content, $args[0], 0, [$args[1]] );		
					}

				break;



				// Insert content before all existing content.
				case 'prepend':

					$this->insert( 0, $args );

				break;



				// Empties the attributes array.
				case 'clearAttributes':

					$this->attributes = [];

				break;



				// Set the attributes.
				case 'setAttributes':

					if( count( $args ) == 1 and is_array( $args[0] ) )
					{
						$this->clearAttributes();

						foreach( $args[0] as $name => $value )
						{
							$this->setAttribute( $name, $value );
						}
					}

				break;



				// Add a class attribute
				case 'addClass':

					if( count( $args ) == 1 )
					{
						if( !is_array( $args[0] ) )
						{
							$args[0] = explode(' ', $args[0] );
						}

						if( !$this->hasAttribute('class') )
						{
							$this->setAttribute( 'class', $args[0] );
						}
						else
						{
							$this->setAttribute( 'class', array_unique( array_merge( $this->getAttribute('class', true), $args[0] ) ) );
						}
					}

				break;



				// Merge passed attributes with existing.
				case 'mergeAttributes':

					if( count( $args ) == 1 and is_array( $args[0] ) )
					{
						$this->attributes = array_merge( $this->attributes, $args[0] );
					}

				break;



				// Set a particular attribute.
				case 'setAttribute':
				case 'attr':

					if( isset( $args[0] ) and isset( $args[1] ) and !empty( $args[0] ) )
					{
						// Should value be stored as an array?
						if( !is_array( $args[1] ) )
						{
							$arr = explode(' ', $args[1] );

							if( count($arr) > 1 )
							{
								$args[1] = $arr;
							}
						}

						$this->attributes[ $args[0] ] = $args[1];
					}

				break;



				// Set the Tag name.
				case 'setName':

					if( count( $args ) == 1 and !empty( $args[0] ) )
					{
						$args[0] = trim(strtolower( $args[0] ));

						$args[0] = preg_replace( '/(\W|\s)/', '', $args[0]);

						if( !empty( $args[0] ) )
						{
							$this->name = $args[0];
						}
					}

				break;



				// Set whether to include a slash before closing > in void elements 
				// for compatibility with XHTML.
				case 'setXHTMLCompatible':

					if( count( $args ) == 1 )
					{
						$this->xhtml_compatible = boolval( $args[0] );
					}

				break;



				// Set whether to escape content 
				case 'setEscContent':

					if( count( $args ) == 1 )
					{
						$this->esc_content = boolval( $args[0] );
					}

				break;



				default:
					throw new \BadMethodCallException( "Method '{$name}' does not exist." );
				break;
			}		
		}

		// Chainable
		return $this;
	}



	/**
	 * Apply method with arguments to child tags that match
	 * the current set of filters. Assumes filters have been
	 * set.
	 * 
	 * @param  string $method
	 * @param  array  $args
	 */
	protected function applyToChildren( $method, $args )
	{
		extract( $this->filters );

		$children = $this->find( $name, $attributes );

		foreach( $children as $child )
		{
			call_user_func_array([ $child, $method ], $args );
		}
	}



	/**
	 * Repeatedly add a tag.
	 * 
	 * @param  string  $tag              [description]
	 * @param  array   $repeat_data      [description]
	 * @param  boolean $merge_attributes [description]
	 * @return [type]                    [description]
	 */
	public function repeat( $tag = '', $repeat_data = [], $merge_attributes = true )
	{
		if( empty( $tag ) or empty( $repeat_data ) )
		{
			return $this;
		}

		if( $this->hasFilters() )
		{
			$this->applyToChildren( 'repeat', [ $tag, $repeat_data, $merge_attributes ] );
		}

		if( is_string($tag) )
		{
			$tag = new Tag( $tag );
		}

		if( $tag instanceof Tag )
		{
			foreach( $repeat_data as $args )
			{
				$clone = clone $tag;

				// Data is a string or tag object
				if( is_string( $args ) or ( $args instanceof Tag ) )
				{
					$clone->setContent( $args );
				}
				// Data is an array including an array of attributes
				elseif( is_array( $args ) )
				{
					if( is_array( $args[0] ) and $merge_attributes )
					{
						$clone->setAttributes( array_merge_recursive( $tag->getAttributes( false ), $args[0] ) );
					}
					elseif( is_array( $args[0] ) )
					{
						$clone->setAttributes( array_merge( $tag->getAttributes( false ), $args[0] ) );
					}

					if( is_string( $args[1] ) )
					{
						$clone->setContent( $args[1] );
					}
				}

				$this->add( $clone );
			}
		}

		return $this;
	}






	/////////////////////////////////////////////////////
	// Finding/ filtering
	/////////////////////////////////////////////////////



	/**
	 * Set a filter to apply methods to child tags.
	 * Returns object for chaining subsequent methods.
	 *  
	 * @return $this
	 */
	public function filter()
	{
		$this->filters = $this->parseFilters( func_get_args() );

		// Chainable
		return $this;
	}



	/**
	 * Clear any filters applied to the object.
	 * @return $this
	 */
	public function clearFilters()
	{
		$this->filter();
		
		// Chainable
		return $this;
	}



	/**
	 * Finds child tags inside this tag that match $name and $attributes.
	 * 
	 * @param  string $name       Tag name
	 * @param  array  $attributes Tag attributes
	 * @param  array  $results    Passed to internal tags
	 * @param  array  $level      Passed to internal tags
	 * @return array              Array of arrays
	 */
	public function find( $name = '', $attributes = [], $results = [] )
	{
		foreach( $this->content as $key => $content )
		{
			if( $content instanceof Tag )
			{
				if( $content->matches( $name, $attributes ) )
				{
					$results[] = $content;
				}		

				$results = call_user_func([$content, 'find'], $name, $attributes, $results );
			}
		}

		return $results;
	}






	/////////////////////////////////////////////////////
	// Parsing arguments
	/////////////////////////////////////////////////////



	/**
	 * Parse arguments provided to constructor or set method.
	 * Allows:
	 * 	1 argument = name of tag
	 * 	2 arguments = name of tag, array of attributes or name of tag, string of content
	 * 	3 arguments = name of tag, array of attributes, string of content
	 *
	 * Also, if any argument is a boolean, this sets the xhtml_compatible property.
	 * 
	 * @param  array  $args [description]
	 * @return null
	 */
	protected function parseArguements( $args = [] )
	{
		/*
			Hook to allow modification to arguments before parsing
		 */
		$args = $this->doHook( 'before_parse_arguements', $args );



		if( empty( $args ) )
		{
			return;
		}

		foreach( $args as $key => $arg )
		{
			if( is_bool( $arg ) )
			{
				$this->setXHTMLCompatible( $arg );
				unset( $args[ $key ] );
			}
		}

		switch( count($args) )
		{
			case 1:
				$this->setName( $args[0] );
			break;
			
			case 2:
				$this->setName( $args[0] );

				if( is_array( $args[1] ) )
				{
					$this->mergeAttributes( $args[1] );
				}
				else
				{
					$this->setContent( $args[1] );
				}
			break;
			
			case 3:
				$this->setName( $args[0] );

				$this->mergeAttributes( $args[1] );

				$this->setContent( $args[2] );
			break;
			
			default:
				throw new \InvalidArgumentException( 'Invalid Tag arguments.' );
			break;
		}
	}



	/**
	 * Internal method for parsing filter arguments.
	 * 
	 * @param  [type] $args [description]
	 * @return [type]       [description]
	 */
	protected function parseFilters( $args )
	{
		$result = 
		[
			'name' => '',
			'attributes' => [],
		];

		switch( count($args) )
		{
			case '1':
				if( is_string( $args[0]) )
				{
					$result['name'] = $args[0];
				}
				elseif( is_array($args[0]) )
				{
					$result['attributes'] = $args[0];
				}
			break;
			
			case '2':
				$result['name'] = $args[0];
				$result['attributes'] = $args[1];
			break;
		}

		return $result;
	}



	/**
	 * Convert array of args into items for content array.
	 * 
	 * @param  array  $args
	 * @return mixed       	Tag object, array of strings, null
	 */
	protected function convertContent( $args = [] )
	{
		if( is_array( $args ) )
		{
			$esc = $this->esc_content;

			// Check for boolean setting whether to escape or not
			foreach( $args as $key => $arg )
			{
				if( is_bool( $arg ) )
				{
					$esc = $arg;
					unset($args[$key]);
				}
			}

			if( count($args) == 1 )
			{
				// A Tag object
				if( $args[0] instanceof Tag )
				{
					return $args[0];
				}
				// An array
				elseif( is_array( $args[0] ) )
				{
					$this->addMultiple( $args[0] );
				}
				// Convert to string
				else
				{
					return [ (string) $args[0], $esc ];
				}
			}
			else
			{
				// Create a Tag object
				$tag = new Tag;

				call_user_func_array([ $tag, 'set'], $args );

				return $tag;
			}
		}
	}






	/////////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////////



	/**
	 * Gets the attributes as a string of HTML style attributes by default, or as an array
	 * @param  boolean $as_string 
	 * @return string/array
	 */
	public function getAttributes( $as_string = true, $sort = false )
	{
		if( $sort )
		{
			ksort( $this->attributes );
		}

		if( $as_string )
		{
			return $this->arrayToAttributes( $this->attributes );
		}

		return $this->attributes;
	}



	/**
	 * Return a particular attribute, or null if it doesn't exist.
	 * 
	 * @param  string  $name      Attribute name
	 * @param  boolean $as_array  Whether to return the attribute as an array
	 * @param  string  $seperator Seperator used to split attribute into an array
	 * @return mixed
	 */
	public function getAttribute( $name = '', $as_array = false, $seperator = ' ' )
	{
		if( isset( $this->attributes[ $name ] ) )
		{
			if( $as_array and !is_array( $this->attributes[ $name ] ))
			{
				return explode( $seperator, $this->attributes[ $name ] );
			}
			else
			{
				return $this->attributes[ $name ];
			}
		}

		return null;
	}



	/**
	 * Returns the content of the tag.
	 * 	
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}



	/**
	 * Returns the name of the tag.
	 * 	
	 * @return string
	 */
	public function getName()
	{
		return strtolower( $this->name );
	}



	/**
	 * Returns whether void element includes /> to be compatible with xhtml.
	 * 	
	 * @return string
	 */
	public function getXHTMLCompatible()
	{
		return $this->xhtml_compatible;
	}



	public function getFilters()
	{
		return $this->filters;
	}



	public function getEscContent()
	{
		return $this->esc_content;
	}







	/////////////////////////////////////////////////////
	// Boolean tests
	/////////////////////////////////////////////////////



	/**
	 * Returns true if the element has no content, or if it is a void element, has an empty value attribute.
	 * 
	 * @return boolean
	 */
	public function isEmpty()
	{
		if(
			( $this->isVoid() and empty( $this->getAttribute( 'value' ) ) )
			or
			( !$this->isVoid() and $this->content === [] )
		)
		{
			return true;
		}

		return false;
	}



	/**
	 * Returns true if the tag is in the list of void elements.
	 * 
	 * @return boolean
	 */
	public function isVoid()
	{
		return in_array( $this->name, $this->void_elements );
	}



	/**
	 * Returns true if the tag should be XHTML compatible.
	 * 
	 * @return boolean
	 */
	public function isXHTMLCompatible()
	{
		return $this->xhtml_compatible;
	}



	/**
	 * Returns true if the tag has attributes.
	 * 
	 * @return boolean
	 */
	public function hasAttributes()
	{
		if( empty($this->attributes) )
		{
			return false;
		}

		return true;
	}



	/**
	 * Returns true if the tag has the attribute.
	 * 
	 * @param  string  $attribute
	 * @return boolean
	 */
	public function hasAttribute( $attribute = '' )
	{
		if( !empty($attribute) and array_key_exists( $attribute, $this->attributes) )
		{
			return true;
		}

		return false;
	}



	/**
	 * Returns true if an attribute has the value specified.
	 * If $order is true, it will only return true if array values are in the same order.
	 * 
	 * @param  string 	$name  	Attribute name
	 * @param  mixed 	$value 	Value(s) to compare against
	 * @param  bool 	$order 	Whether the order of an array is important
	 * @return bool
	 */
	public function attributeEquals( $name, $value = '', $order = false )
	{
		if( !$this->hasAttribute( $name ) )
		{
			return false;
		}

		$current = $this->getAttribute( $name );

		// Make value an array if it needs to be
		if( is_array($this->getAttribute( $name )) and !is_array($value) )
		{
			// Split a string
			if( is_string($value) )
			{
				$value = explode(' ', $value);
			}
			else
			{
				$value = [$value];
			}
		}

		if( !$order and is_array($current) and is_array($value) )
		{
			sort($current);
			sort($value);
		}

		return $current === $value;
	}



	public function attributeHasValue( $name, $value = '' )
	{
		if( !$this->hasAttribute( $name ) )
		{
			return false;
		}

		$current = $this->getAttribute( $name );

		// If the current value is not an array make it one
		if( !is_array($current) )
		{
			$current = explode(' ', $current);
		}

		// Make value an array if it isn't
		if( !is_array($value) )
		{
			$value = explode(' ', $value);
		}

		if( !empty(array_diff( $value, $current )) )
		{
			return false;
		}

		return true;
	}



	/**
	 * Returns true if this tag matches the name and/or attributes.
	 * 
	 * @param  string $name       [description]
	 * @param  array  $attributes [description]
	 * @return [type]             [description]
	 */
	public function matches()
	{
		extract( $this->parseFilters( func_get_args() ) );

		if( $name != '' and $this->getName() !== $name )
		{
			return false;
		}

		if( is_array($attributes) )
		{
			foreach( $attributes as $key => $value )
			{
				if( !$this->attributeHasValue( $key, $value ) )
				{
					return false;
				}
			}
		}

		return true;
	}



	public function hasFilters()
	{
		if(
			(isset( $this->filters['name'] ) and $this->filters['name'] !== '' )
			or
			(isset( $this->filters['attributes'] ) and $this->filters['attributes'] !== [])
		)
		{
			return true;
		}

		return false;
	}





	/////////////////////////////////////////////////////
	// Rendering
	/////////////////////////////////////////////////////



	/**
	 * Render (echo) the Tag, or return it if $echo is false.
	 * @param  boolean $echo
	 * @return null/string
	 */
	public function render( $echo = true )
	{
		// Remove in later version? Replaced by Hooks
		$this->beforeRender();



		/*
			Hook to allow modification to tag object just before rendering
		 */
		$this->doHook( 'before_render', $this );



		/*
			Hook to allow modification to output string before rendering
		 */
		$html = $this->doHook( 'initial_output_string', '', $this );



		// Start tag
		if( strlen( $this->getName() ) > 0 )
		{
			$html .= "<{$this->getName()}{$this->getAttributes( true, true )}";

			if( $this->isVoid() and $this->isXHTMLCompatible() )
			{
				$html .= "/";
			}
			
			$html .= ">";
		}

		// Content
		$html .= $this->renderContent();
		
		// End tag
		if( strlen( $this->getName() ) > 0 and !$this->isVoid() )
		{
			$html .= "</{$this->getName()}>";
		}

		// Hooks for modifying final output
		$html = $this->modifyOutputString( $html );



		/*
			Hook to allow modification to html string before returning
		 */
		$html = $this->doHook( 'output_string', $html );




		// Echo or return
		if( $echo )
		{
			echo $html;
		}
		else
		{
			return $html;
		}
	}



	protected function renderContent()
	{
		$html = '';

		foreach( $this->content as $index => $item )
		{
			if( $item instanceof Tag )
			{
				$html .= $item->render( false );
			}
			elseif( is_array( $item ) )
			{
				// Make it a string
				$item[0] = (string) $item[0];

				if( $item[1] )
				{
					$html .= $this->esc( $item[0] );
				}
				else
				{
					$html .= $item[0];
				}
			}
		}

		return $html;
	}



	/**
	 * Used by extending classes to change defaults.
	 * 
	 * @return [type] [description]
	 */
	public function beforeRender()
	{
	}



	/**
	 * Used by extending classes to modify final output string.
	 * 
	 * @param  string $html From render method
	 * @return string       Modified HTML
	 */
	public function modifyOutputString( $html )
	{
		return $html;
	}







	/////////////////////////////////////////////////////
	// Utilities
	/////////////////////////////////////////////////////



	/**
	 * Turns an array into a string of html style attributes.
	 * A value which is an array will be converted into a space-seperated string (e.g. a list of CSS classes).
	 * A value which is a boolean will only be used if true (e.g. selected="selected").
	 * A numerically indexed item in an array will result in only the value being used (e.g. checked)
	 * @param  array  $arr 
	 * @return string    	  Attributes, e.g. class="my_class" type="text"
	 */
	protected function arrayToAttributes( $arr = [] )
	{
		$output = [];

		foreach( $arr as $name => $value )
		{
			// Convert array value to space seperated string (e.g. classes)
			if( is_array( $value ) )
			{
				$value = implode( ' ', $value );
			}

			// Deal with boolean values
			if( $value === true )
			{
				$value = $name;
			}

			// Add value if it is a string
			if( is_string($name) and $value !== false )
			{
				$output[] = "$name=\"{$this->esc($value)}\"";
			}
			elseif( is_string($value) )
			{
				$output[] = "{$this->esc($value)}";
			}
		}

		if( !empty( $output ) )
		{
			return " " . implode( ' ', $output );
		}

		return '';
	}



	/**
	 * Escape special characters in a string.
	 * 
	 * @param  string $string [description]
	 * @return [type]         [description]
	 */
	protected function esc( $string = '' )
	{
		$string = strval( $string );

		// Convert special characters into entities
		return htmlspecialchars( $string, ENT_QUOTES, $this->encoding );
	}

}